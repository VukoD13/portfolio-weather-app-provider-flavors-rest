This is a simple weather app to show the use of Provider, Flavors (Schemes on iOS), and REST API.

to launch the app offline with mocked data, use:
flutter run --flavor dev -t lib/main.dev.dart

to launch the app online and use REST API, use:
flutter run --flavor prod -t lib/main.prod.dart

Feel free to contact me: dawid_wnukowski@yahoo.com (especially if you need a Flutter Developer)