import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:task/models/single_day_model.dart';
import 'package:task/services/weather_service.dart';

class WeatherServiceProd extends WeatherService {
  static const String _baseUrl = 'https://www.metaweather.com/';
  static const String _weatherRequestUrl = 'api/location/';
  static const String _locationId = '44418';

  static const String _consolidatedWeatherKey = 'consolidated_weather';

  String _buildFullLocationDataPath(String locationId) {
    return _baseUrl + _weatherRequestUrl + locationId;
  }

  @override
  Future<List<SingleDayModel>> fetchWeatherForLocation() async {
    final String path = _buildFullLocationDataPath(_locationId);
    final http.Response response = await http.get(path);
    final List<dynamic> data = json.decode(response.body)[_consolidatedWeatherKey];

    final List<SingleDayModel> result = [];
    data.forEach((element) => result.add(SingleDayModel.fromMap(element)));

    return result;
  }
}