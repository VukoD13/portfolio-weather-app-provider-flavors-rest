import 'package:task/models/single_day_model.dart';

abstract class WeatherService {
  Future<List<SingleDayModel>> fetchWeatherForLocation();
}