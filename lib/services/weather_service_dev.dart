import 'package:task/models/single_day_model.dart';
import 'package:task/services/weather_service.dart';

class WeatherServiceDev extends WeatherService {
  @override
  Future<List<SingleDayModel>> fetchWeatherForLocation() {
    final List<SingleDayModel> result = List();
    for (int i = 0; i < 7; i++) {
      result.add(
        SingleDayModel(
          date: DateTime.now().add(Duration(days: i)),
          theTemperature: 20.0 + i,
          weatherStateAbbr: '',
          maxTemperature: 30.0 + i,
          airPressure: 1000,
          weatherStateName: 'Rain',
          windSpeed: 1001,
          minTemperature: 10.0 + i,
          humidity: 1002,
        ),
      );
    }
    // in some cases it may be good idea to add delay to test loading state -> Future.delayed
    return Future.value(result);
  }
}
