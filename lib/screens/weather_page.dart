import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:task/app_config.dart';
import 'package:task/screens/single_day_card.dart';
import 'package:task/screens/weather_view_model.dart';

class WeatherPage extends StatefulWidget {
  @override
  _WeatherPageState createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  WeatherViewModel viewModel;
  Future<void> fetchWeatherData;

  @override
  void initState() {
    super.initState();
    viewModel = Provider.of<WeatherViewModel>(context, listen: false);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      fetchWeatherData = viewModel.fetchWeather();
    });
  }

  void _onRetryPressed() {
    fetchWeatherData = viewModel.fetchWeather();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: _buildPage(),
      ),
    );
  }

  Widget _buildPage() {
    final bool isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return Consumer<WeatherViewModel>(
      builder: (context, viewModel, _) {
        if (viewModel.isLoading) {
          return _buildLoader();
        } else if (viewModel.isError) {
          return _buildRetryButton();
        }
        return Padding(
          padding: const EdgeInsets.all(16.0),
          child: RefreshIndicator(
            child: isPortrait ? _buildPortraitPage() : _buildLandscapePage(),
            onRefresh: () => viewModel.fetchWeather(),
          ),
        );
      },
    );
  }

  Widget _buildPortraitPage() {
    return ListView(
      shrinkWrap: true,
      children: [
        _buildHeader(),
        _buildIcon(MediaQuery.of(context).size.width * 0.55),
        _buildTemperature(),
        _buildWeatherDetails(),
        _buildDays(),
      ],
    );
  }

  Widget _buildLandscapePage() {
    return Row(
      children: [
        _buildDays(scrollDirection: Axis.vertical),
        Expanded(
          child: Column(
            children: [
              _buildHeader(crossAxisAlignment: CrossAxisAlignment.center),
              _buildIcon(MediaQuery.of(context).size.height * 0.55),
            ],
          ),
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _buildTemperature(),
              _buildWeatherDetails(isPortrait: false),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildLoader() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _buildRetryButton() {
    return Center(
      child: RaisedButton(
        child: Text(
          'Retry',
        ),
        onPressed: _onRetryPressed,
      ),
    );
  }

  Widget _buildTemperature() {
    return Center(
      child: Text(
        (viewModel?.selectedDay?.theTemperature?.round()?.toString() ?? '-') + '°',
        style: Theme.of(context).textTheme.headline1,
      ),
    );
  }

  Widget _buildIcon(double size) {
    final bool useMockedData = Provider.of<AppConfig>(context).useMockedData;
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: viewModel.selectedDay?.imageUrl == null || useMockedData
          ? Container(
              width: size,
              height: size,
            )
          : SvgPicture.network(
              viewModel.selectedDay.imageUrl,
              width: size,
              height: size,
            ),
    );
  }

  Widget _buildHeader(
      {CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.start}) {
    return Column(
      crossAxisAlignment: crossAxisAlignment,
      children: [
        Center(
          child: Text(
            viewModel.selectedDay?.weekdayName ?? '-',
            style: Theme.of(context).textTheme.headline2,
          ),
        ),
        const SizedBox(height: 8),
        Text(
          viewModel.selectedDay?.weatherStateName ?? '-',
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ],
    );
  }

  Widget _buildWeatherDetails({bool isPortrait = true}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Column(
        crossAxisAlignment:
            isPortrait ? CrossAxisAlignment.stretch : CrossAxisAlignment.start,
        children: [
          Text(
            'Humidity: ${viewModel?.selectedDay?.humidity ?? '-'}%',
            style: Theme.of(context).textTheme.bodyText1,
          ),
          const SizedBox(height: 6),
          Text(
            'Pressure: ${viewModel?.selectedDay?.airPressure?.round() ?? '-'} hPa',
            style: Theme.of(context).textTheme.bodyText1,
          ),
          const SizedBox(height: 6),
          Text(
            'Wind: ${viewModel?.selectedDay?.windSpeed?.round() ?? '-'} km/h',
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ],
      ),
    );
  }

  Widget _buildDays({Axis scrollDirection = Axis.horizontal}) {
    if (scrollDirection == Axis.horizontal) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.25,
        child: _buildDaysList(scrollDirection),
      );
    } else {
      return Container(
        width: MediaQuery.of(context).size.width * 0.25,
        child: _buildDaysList(scrollDirection),
      );
    }
  }

  Widget _buildDaysList(Axis scrollDirection) {
    return ListView(
      scrollDirection: scrollDirection,
      children: List.generate(
        viewModel.data?.length ?? 0,
        (index) => SingleDayCard(
          model: viewModel.data[index],
          onTap: () => viewModel.selectDay(index),
        ),
      ),
    );
  }
}
