import 'package:flutter/foundation.dart';
import 'package:task/models/single_day_model.dart';
import 'package:task/services/weather_service.dart';

class WeatherViewModel extends ChangeNotifier {
  final WeatherService _weatherService;

  WeatherViewModel(this._weatherService);

  List<SingleDayModel> _data;

  List<SingleDayModel> get data => _data;

  SingleDayModel _selectedDay;

  SingleDayModel get selectedDay => _selectedDay;

  bool _isLoading = false;

  bool get isLoading => _isLoading;

  bool _isError = false;

  bool get isError => _isError;

  Future<void> fetchWeather() async {
    _isLoading = true;
    notifyListeners();

    try {
      // only for testing purposes!
      // if (kDebugMode || kReleaseMode) {
      //   if (Random().nextInt(2) == 0) {
      //     await Future.delayed(const Duration(seconds: 1));
      //     throw Exception('test error');
      //   }
      // }
      //

      _data = await _weatherService.fetchWeatherForLocation();

      // select the first day when _selectedDay doesn't exist in _data anymore, because of some change in data
      if (_data.contains(_selectedDay) == false) {
        selectDay(0);
      }
      _isError = false;
    } catch (e, st) {
      _isError = true;
      // in the 'real' app, I have a habit to use plugins or at least write my own logger so I can control when, how and where the app should print messages
      print(st);
    } finally {
      _isLoading = false;
      notifyListeners();
    }
  }

  void selectDay(int index) {
    final model = _data[index];
    if (model != _selectedDay) {
      _selectedDay = model;
      notifyListeners();
    }
  }
}
