import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:task/app_config.dart';
import 'package:task/models/single_day_model.dart';

class SingleDayCard extends StatelessWidget {
  final SingleDayModel model;
  final GestureTapCallback onTap;

  const SingleDayCard({
    Key key,
    @required this.model,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
        child: model == null ? Container() : _buildCardContent(context),
      ),
      onTap: onTap,
    );
  }

  Widget _buildCardContent(BuildContext context) {
    final bool useMockedData = Provider.of<AppConfig>(context).useMockedData;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Center(
            child: Text(
              model.shortWeekdayName,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          model.imageUrl == null || useMockedData
              ? Container(
                  width: 32,
                  height: 32,
                )
              : SvgPicture.network(
                  model.imageUrl,
                  width: 32,
                  height: 32,
                ),
          Center(
            child: Text(
              '${model.minTemperature.round()}° / ${model.maxTemperature.round()}°',
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
        ],
      ),
    );
  }
}
