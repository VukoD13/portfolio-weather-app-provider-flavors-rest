import 'package:flutter/foundation.dart';

class AppConfig {
  final Flavor flavor;
  final bool useMockedData;

  AppConfig({
    @required this.flavor,
    @required this.useMockedData,
  });
}

enum Flavor {
  dev,
  prod,
}
