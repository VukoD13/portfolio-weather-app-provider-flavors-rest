import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class SingleDayModel {
  static const String baseImagePath = 'https://www.metaweather.com/static/img/weather/';

  static const String minTempKey = 'min_temp';
  static const String maxTempKey = 'max_temp';
  static const String theTempKey = 'the_temp';
  static const String humidityKey = 'humidity';
  static const String airPressureKey = 'air_pressure';
  static const String windSpeedKey = 'wind_speed';
  static const String weatherStateNameKey = 'weather_state_name';
  static const String weatherStateAbbrKey = 'weather_state_abbr';
  static const String applicableDateKey = 'applicable_date';

  double minTemperature, maxTemperature, theTemperature;
  double airPressure, windSpeed;
  int humidity;
  String weatherStateName, weatherStateAbbr;
  DateTime date;

  String weekdayName, shortWeekdayName;
  String imageUrl;

  SingleDayModel({
    @required this.humidity,
    @required this.minTemperature,
    @required this.maxTemperature,
    @required this.theTemperature,
    @required this.airPressure,
    @required this.windSpeed,
    @required this.weatherStateName,
    @required this.weatherStateAbbr,
    @required this.date,
  }) {
    weekdayName = buildWeekdayName();
    shortWeekdayName = buildShortWeekdayName();
    imageUrl = buildImageUrl();
  }

  factory SingleDayModel.fromMap(Map<dynamic, dynamic> data) {
    if (data == null) {
      return null;
    }

    return SingleDayModel(
      humidity: data[humidityKey],
      minTemperature: (data[minTempKey]),
      maxTemperature: data[maxTempKey],
      theTemperature: data[theTempKey],
      airPressure: data[airPressureKey],
      windSpeed: data[windSpeedKey],
      weatherStateName: data[weatherStateNameKey],
      weatherStateAbbr: data[weatherStateAbbrKey],
      date: DateTime.parse(data[applicableDateKey]),
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SingleDayModel &&
          runtimeType == other.runtimeType &&
          humidity == other.humidity &&
          minTemperature == other.minTemperature &&
          maxTemperature == other.maxTemperature &&
          theTemperature == other.theTemperature &&
          airPressure == other.airPressure &&
          windSpeed == other.windSpeed &&
          weatherStateName == other.weatherStateName &&
          weatherStateAbbr == other.weatherStateAbbr &&
          date == other.date &&
          weekdayName == other.weekdayName &&
          shortWeekdayName == other.shortWeekdayName &&
          imageUrl == other.imageUrl;

  @override
  int get hashCode => hashValues(
        humidity,
        minTemperature,
        maxTemperature,
        theTemperature,
        airPressure,
        windSpeed,
        weatherStateName,
        weatherStateAbbr,
        date,
        weekdayName,
        shortWeekdayName,
        imageUrl,
      );

  @override
  String toString() {
    return 'WeatherLocationDataModel{humidity: $humidity, minTemperature: $minTemperature, maxTemperature: $maxTemperature, theTemperature: $theTemperature, airPressure: $airPressure, windSpeed: $windSpeed, weatherStateName: $weatherStateName, weatherStateAbbr: $weatherStateAbbr, date: $date, weekdayName: $weekdayName, shortWeekdayName: $shortWeekdayName, imageUrl: $imageUrl}';
  }

  @visibleForTesting
  String buildShortWeekdayName() => weekdayName.substring(0, 3);

  @visibleForTesting
  String buildWeekdayName() => DateFormat('EEEEE', 'en_US').format(date);

  @visibleForTesting
  String buildImageUrl() => '$baseImagePath$weatherStateAbbr.svg';
}
