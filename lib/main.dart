import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:task/app_config.dart';
import 'package:task/constants/app_theme.dart';
import 'package:task/screens/weather_page.dart';
import 'package:task/screens/weather_view_model.dart';
import 'package:task/services/weather_service_dev.dart';
import 'package:task/services/weather_service_prod.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AppConfig appConfig = Provider.of<AppConfig>(context);

    return MaterialApp(
      title: 'Simple Weather App',
      debugShowCheckedModeBanner: !kReleaseMode,
      theme: AppTheme.theme(appConfig.flavor),
      home: ChangeNotifierProvider(
        create: (_) => WeatherViewModel(
          appConfig.useMockedData ? WeatherServiceDev() : WeatherServiceProd(),
        ),
        builder: (context, _) => WeatherPage(),
      ),
    );
  }
}
