import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:task/app_config.dart';
import 'package:task/main.dart';

Future<void> main() async {
  runApp(
    Provider(
      create: (_) => AppConfig(
        flavor: Flavor.prod,
        useMockedData: false,
      ),
      child: MyApp(),
    ),
  );
}
