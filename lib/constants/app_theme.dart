import 'package:flutter/material.dart';
import 'package:task/app_config.dart';

class AppTheme {
  static ThemeData theme([Flavor flavor = Flavor.prod]) {
    final ThemeData base = ThemeData.dark();

    return base.copyWith(
      primaryColor: flavor == Flavor.dev ? Colors.purple : Colors.green,
      textTheme: TextTheme(
        headline1: const TextStyle(
          fontSize: 40,
        ),
        headline2: const TextStyle(
          fontSize: 24,
        ),
        subtitle1: const TextStyle(
          fontSize: 17,
        ),
        bodyText1: const TextStyle(
          fontSize: 15,
        ),
      ),
    );
  }
}
