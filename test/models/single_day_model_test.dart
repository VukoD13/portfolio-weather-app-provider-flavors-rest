import 'package:flutter_test/flutter_test.dart';
import 'package:task/models/single_day_model.dart';

void main() {
  group('fromMap', () {
    test('null data', () {
      final SingleDayModel model = SingleDayModel.fromMap(null);
      expect(model, null);
    });

    test('SingleDayModel with all properties', () {
      final SingleDayModel model = SingleDayModel.fromMap({
        SingleDayModel.minTempKey: 10.0,
        SingleDayModel.maxTempKey: 20.0,
        SingleDayModel.theTempKey: 17.0,
        SingleDayModel.humidityKey: 1000,
        SingleDayModel.airPressureKey: 1001.0,
        SingleDayModel.windSpeedKey: 1002.0,
        SingleDayModel.weatherStateNameKey: 'Rain',
        SingleDayModel.weatherStateAbbrKey: 'rain',
        SingleDayModel.applicableDateKey: '2020-09-24',
      });
      expect(
          model,
          SingleDayModel(
            humidity: 1000,
            minTemperature: 10.0,
            maxTemperature: 20.0,
            theTemperature: 17.0,
            airPressure: 1001.0,
            windSpeed: 1002.0,
            weatherStateName: 'Rain',
            weatherStateAbbr: 'rain',
            date: DateTime.parse('2020-09-24'),
          ));
    });

    test('missing humidity', () {
      final SingleDayModel model = SingleDayModel.fromMap({
        SingleDayModel.minTempKey: 10.0,
        SingleDayModel.maxTempKey: 20.0,
        SingleDayModel.theTempKey: 17.0,
        SingleDayModel.airPressureKey: 1001.0,
        SingleDayModel.windSpeedKey: 1002.0,
        SingleDayModel.weatherStateNameKey: 'Rain',
        SingleDayModel.weatherStateAbbrKey: 'rain',
        SingleDayModel.applicableDateKey: '2020-09-24',
      });
      expect(model.humidity, null);
    });
  });

  group('buildShortWeekdayName', () {
    test('valid properties', () {
      final model = SingleDayModel(
        humidity: 1000,
        minTemperature: 10.0,
        maxTemperature: 20.0,
        theTemperature: 17.0,
        airPressure: 1001.0,
        windSpeed: 1002.0,
        weatherStateName: 'Rain',
        weatherStateAbbr: 'rain',
        date: DateTime.parse('2020-09-24'),
      );
      expect(model.buildShortWeekdayName(), "Thu");
    });
  });

  group('buildImageUrl', () {
    test('valid properties', () {
      final model = SingleDayModel(
        humidity: 1000,
        minTemperature: 10.0,
        maxTemperature: 20.0,
        theTemperature: 17.0,
        airPressure: 1001.0,
        windSpeed: 1002.0,
        weatherStateName: 'Rain',
        weatherStateAbbr: 'rain',
        date: DateTime.parse('2020-09-24'),
      );
      expect(model.buildImageUrl(), '${SingleDayModel.baseImagePath}rain.svg');
    });
  });

  group('buildWeekdayName', () {
    test('valid properties', () {
      final model = SingleDayModel(
        humidity: 1000,
        minTemperature: 10.0,
        maxTemperature: 20.0,
        theTemperature: 17.0,
        airPressure: 1001.0,
        windSpeed: 1002.0,
        weatherStateName: 'Rain',
        weatherStateAbbr: 'rain',
        date: DateTime.parse('2020-09-24'),
      );
      expect(model.buildWeekdayName(), "Thursday");
    });
  });
}
